let currentInput = "zéro";
let lastOperation = "";
let lastResult = 0;

function afiche() {
  document.getElementById("box").innerText = currentInput;
}
function button_number(num) {
  if (currentInput === "zéro") {
    currentInput = num.toString();
  } else {
    currentInput += num.toString();
  }
  afiche();
}
function button_clear() {
  currentInput = "zéro";
  lastOperation = "";
  lastResult = 0;
  afiche();
}

function clear_entry() {
  currentInput = currentInput.slice(0, -1);
  if (currentInput === "") {
    currentInput = "zéro";
  }
  afiche()
}

function calculate_percentage() {
    currentInput = (parseFloat(currentInput) / 100).toString();
    afiche();
}
function division_one() {
    currentInput = (1 / parseFloat(currentInput)).toString();
    afiche();
}
function power_of() {
    currentInput = (parseFloat(currentInput) ** 2).toString();
    afiche();
}
function square_root() {
    currentInput = Math.sqrt(parseFloat(currentInput)).toString();
    afiche();
}
function plus_minus() {
    currentInput = (parseFloat(currentInput) * -1).toString();
    afiche();
}
function performOperation() {
    let result;
    const currentNumber = parseFloat(currentInput);

    switch (lastOperation) {
        case '+':
            result = lastResult + currentNumber;
            break;
        case '-':
            result = lastResult - currentNumber;
            break;
        case '*':
            result = lastResult * currentNumber;
            break;
        case '/':
            result = lastResult / currentNumber;
            break;
        default:
            result = currentNumber;
    }

    currentInput = result.toString()
    lastResult = result;
    lastOperation = '';
}

function button_resultat(operator) {
    if (operator === '=') {
        if (lastOperation !== '') {
            lastOperation = '';
            document.getElementById('box').innerText = '';
        }
    } else {

            lastResult = parseFloat(currentInput);
        

        lastOperation = operator;
        currentInput = '0';
        document.getElementById('last_operation_history').innerText = lastResult + ' ' + operator;
    }

    afiche();
}